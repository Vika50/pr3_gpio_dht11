from django.shortcuts import render

# Create your views here.

import os
from django.views import generic
from django.conf import settings

from djangoapp.models import Measurement
from djangoapp.models import Indications

from django.urls import reverse_lazy
from djangoapp.forms import MeasurementForm



class MeasurementList(generic.ListView):
    model = Measurement
    context_object_name = 'measurements'
    template_name = 'measurement_list.html'


    def get(self, request, *args, **kwargs):
        try:
            file_path = os.path.join(settings.BASE_DIR, 'test_date')
            measurement_file = open(file_path, 'r')
            for value in measurement_file:
                Measurement.objects.get_or_create(value=value)
        except IOError:
            pass
        return super(MeasurementList, self).get(request, *args, **kwargs)


class MeasurementCreate(generic.CreateView):
    form_class = MeasurementForm
    template_name = 'create_measurement.html'
    success_url = reverse_lazy('measurement_list')



class IndicationsList(generic.ListView):
    model = Indications
    context_object_name = 'indications'
    template_name = 'view_indications.html'
	 

    def get(self, request, *args, **kwargs):
        try:
            file_path = os.path.join(settings.BASE_DIR, 'data')
            indication_file = open(file_path, 'r')
            for temperature in indication_file:
                Indications.objects.get_or_create(temperature=temperature)
        except IOError:
            pass
        return super(IndicationsList, self).get(request, *args, **kwargs)





	 