from __future__ import unicode_literals
from django.db import models

# Create your models here.

class Measurement(models.Model):
    value = models.CharField(max_length=50)
    description = models.CharField( max_length=250, blank=True, null=True)
    autor = models.CharField( max_length=250, blank=True, null=True)
    humidity = models.CharField( max_length=250, blank=True, null=True)


    def __unicode__(self):
        return self.value

        
class Indications(models.Model):
	 temperature = models.CharField(max_length=50)
	 humidity = models.CharField(max_length=50)
	 

	         
     