import RPi.GPIO as GPIO
from time import sleep

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BOARD)
arr = [7, 8, 18, 16, 15, 13, 12, 11]

while True:
   for i in arr:
      GPIO.setup(i, GPIO.OUT)
      GPIO.output(i, True)
      sleep(0.5)
   for i in arr:
      GPIO.setup(i, GPIO.OUT)
      GPIO.output(i, False)
      sleep(0.5)
